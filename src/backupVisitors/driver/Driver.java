package backupVisitors.driver;

import backupVisitors.myTree.BST;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.Logger;
import backupVisitors.util.MyLogger;
import backupVisitors.util.Results;
import backupVisitors.visitors.FullTimeStatusVisitor;
import backupVisitors.visitors.IdenticalRecordsVisitor;
import backupVisitors.visitors.PopulateTreeVisitor;
import backupVisitors.visitors.SortedRecordsVisitor;
import backupVisitors.visitors.StatsVisitor;
import backupVisitors.visitors.TreeProcessingVisitorI;

public class Driver {

	public static void main(String[] args) {

		String inputFile = null;
		String deleteFile = args[1];
		String stats = args[2];
		String sorted = args[3];
		String identical = args[4];
		BST bst = new BST();
		MyLogger logger = MyLogger.getInstance();

		try {
			inputFile = args[0];
			if (!(inputFile.equals("input.txt"))) {
				System.out.println("No Input File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No Input File Present Please Enter It");
			System.exit(0);
		}
		try {
			deleteFile = args[1];
			if (!(deleteFile.equals("delete.txt"))) {
				System.out.println("No Delete File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No delete File Present Please Enter It");
			System.exit(0);
		}
		try {
			stats = args[2];
			if (!(stats.equals("stats.txt"))) {
				System.out.println("No stats File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No stats File Present Please Enter It");
			System.exit(0);
		}
		try {
			sorted = args[3];
			if (!(sorted.equals("sorted.txt"))) {
				System.out.println("No sorted File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No sorted File Present Please Enter It");
			System.exit(0);
		}
		try {
			identical = args[4];
			if (!(identical.equals("identical.txt"))) {
				System.out.println("No identical File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No identical File Present Please Enter It");
			System.exit(0);
		}

		try {
			if (args[5] != null) {
				if (args.length > 0 && args.length < 7) {
					Logger.setDebugValue(Integer.parseInt(args[5]));
				} else {
					System.out.println("Insufficient argument");
				}
			}
		} catch (Exception e) {
			System.out.println("Insufficient argument");
		}

		// to read from file and apply appropriate patterns
		FileProcessor fp = new FileProcessor(args[0], "test.txt");
		TreeProcessingVisitorI populateTreeVisitor = new PopulateTreeVisitor(fp);
		TreeProcessingVisitorI fullTSV = new FullTimeStatusVisitor(fp);
		TreeProcessingVisitorI stateVisitors = new StatsVisitor(fp);
		TreeProcessingVisitorI sortedVisitors = new SortedRecordsVisitor(fp);
		TreeProcessingVisitorI identicalVisitors = new IdenticalRecordsVisitor(fp);
		Results res = new Results();
		bst.accept(populateTreeVisitor);
		bst.deleteCreateValue(args[1]);
		bst.accept(stateVisitors);
		bst.accept(fullTSV);
		bst.accept(sortedVisitors);
		bst.accept(identicalVisitors);

	}

}
