package backupVisitors.myTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import backupVisitors.util.FileProcessor;
import backupVisitors.util.Logger;
import backupVisitors.util.Logger.DebugLevel;
import backupVisitors.util.MyLogger;
import backupVisitors.util.Results;
import backupVisitors.visitors.TreeProcessingVisitorI;

public class BST {
	Node parentNode;
	MyLogger logger = MyLogger.getInstance();
	public Node root;
	public int averageCourse = 0;
	public int medianCourse = 0;
	public int totalCoursesCount = 0;
	public int i = 0;
	ArrayList<HashMap<String, String>> mal = new ArrayList<>();
	Results res = new Results();
	public HashMap<String, String> reverseSortedArray = new HashMap<String, String>();

	public List sA = new ArrayList<String>();
	public List medianList = new ArrayList<String>();
	public int bnum;
	public List course;

	public BST(int bNum, List course) {
		this.bnum = bNum;
		this.course = course;

	}

	public BST() {
		Logger.writeMessage("BST COnstructor called", DebugLevel.CONSTRUCTOR);
		root = null;
	}

	// insert in node
	public void insert(Node node, String course) {
		List<String> courseList;
		// System.out.println(parentNode);
		Node n = isPresent(node, root);
		if (n == null) {
			if (root == null) {
				courseList = new ArrayList<String>();
				courseList.add(course);
				node.setCourses(courseList);
				root = node;
				return;
			}
			Node currentNode = root;
			parentNode = null;
			courseList = new ArrayList<String>();
			courseList.add(course);
			node.setCourses(courseList);

			while (true) {
				parentNode = currentNode;
				if (node.getbNumber() < currentNode.getbNumber()) {
					currentNode = currentNode.getLeft();
					if (currentNode == null) {
						parentNode.setLeft(node);
						return;
					}
				} else {
					currentNode = currentNode.right;
					if (currentNode == null) {
						parentNode.right = node;
						return;
					}
				}
			}
		} else {
			courseList = n.getCourses();
			courseList.add(course);
			node.setCourses(courseList);
		}
	}

	// check node with less items
	public void checkNodeWithDeficencies(Node catchingRoot) {

		logger.printToLog(3, "Inside checkNodeWithDeficencies() method of BST\n");
		if (catchingRoot != null) {
			if (catchingRoot.getCourses().size() < 3) {
				catchingRoot.getCourses().add("S");
				catchingRoot.notifyObserver(bnum, "S");
			}
			// System.out.println("Course is :" + catchingRoot.getbNumber() + "
			// Courses size is : "
			// + catchingRoot.getCourses().size() + " and courses are : " +
			// catchingRoot.getCourses());

		}
		if (catchingRoot == null) {
			return;
		}
		checkNodeWithDeficencies(catchingRoot.getLeft());
		checkNodeWithDeficencies(catchingRoot.right);
	}

	// calculating the avarage
	public int checkNodeStats(Node root2) {

		logger.printToLog(3, "Inside checkNodeStats() method of BST\n");
		if (root2 == null) {
			return 0;
		}

		return (1 + checkNodeStats(root2.getLeft()) + checkNodeStats(root2.right));

	}

	// sorting the nodes
	public void checkNodeWithSorting(Node catchingRoot) {
		logger.printToLog(3, "Inside checkNodeWithSorting() method of BST\n");

		if (catchingRoot != null) {

			int bn = catchingRoot.getbNumber();
			sA.add(catchingRoot.getbNumber() + " Courses are: " + (catchingRoot.getCourses().toString()));

		}
		if (catchingRoot == null) {
			return;
		}
		checkNodeWithSorting(catchingRoot.getLeft());
		checkNodeWithSorting(catchingRoot.right);

	}

	public void sortingStart() {

		Collections.sort(sA);
		res.printViaStd("\n************SORTED ELEMENTS***********");
		res.printViaFile("\n************SORTED ELEMENTS***********");
		// System.out.println("\n************SORTED ELEMENTS***********");

		for (Object s : sA) {
			res.printViaStd(s.toString());
			res.printViaFile(s.toString());

		}

	}

	// check the median
	public void checkNodeCourseWithMedian(Node catchingRoot) {
		logger.printToLog(3, "Inside checkNodeCourseWithMedian() method of BST\n");

		if (catchingRoot != null) {

			if (medianList.contains(catchingRoot.getCourses())) {
				return;
			} else {
				medianList.add(catchingRoot.getCourses());
			}
		}
		if (catchingRoot == null) {
			return;
		}
		checkNodeCourseWithMedian(catchingRoot.getLeft());
		checkNodeCourseWithMedian(catchingRoot.right);

	}

	public Object calculateMedian() {
		int size = medianList.size();
		// System.out.println(size);
		int s = size / 2;

		Object value = medianList.get(s);
		return value;
	}

	// check course with less elements
	public int checkNodeCoursesStats(Node root2) {
		logger.printToLog(3, "Inside checkNodeCoursesStats() method of BST\n");

		if (root2 == null) {
			return 0;
		}
		return (root2.getCourses().size() + checkNodeCoursesStats(root2.getLeft())
				+ checkNodeCoursesStats(root2.right));

	}

	public void accept(TreeProcessingVisitorI visitor) {
		logger.printToLog(1, "Inside accept() method of BST\n");
		visitor.visit(this);
	}

	// check if the element is present
	private Node isPresent(Node n, Node currentNode) {
		if (currentNode == null) {
			return null;
		}
		if (n.getbNumber() == currentNode.getbNumber()) {
			return currentNode;
		} else if (n.getbNumber() < currentNode.getbNumber()) {
			if (currentNode.getLeft() == null) {
				return null;
			} else {
				return isPresent(n, currentNode.getLeft());
			}
		} else if (n.getbNumber() > currentNode.getbNumber()) {
			if (currentNode.getRight() == null) {
				return null;
			} else {
				return isPresent(n, currentNode.getRight());
			}
		}
		return currentNode;

	}

	// delete from node the values
	public void deleteCreateValue(String deleteFile) {
		int k = 1;
		String str = null;
		FileProcessor fp = new FileProcessor();
		List<String> strVal = new ArrayList<String>();

		while ((str = fp.ReadLine(deleteFile)) != null) {

			int i = 0;
			String course = null;
			Pattern pattern = Pattern.compile("\\w+");
			Matcher matcher = pattern.matcher(str);

			int intVal = 0;

			while (matcher.find()) {

				if (i == 0) {
					intVal = Integer.parseInt(matcher.group());
					i++;
				} else {

					course = matcher.group();
					// strVal.add(matcher.group());
				}

			}
			deleteValues(intVal, course);
		}

	}

	private void deleteValues(int intVal, String course2) {
		Node n = new Node(intVal);
		Node n2 = getRoot();

		// System.out.println("hi");

		Node n1 = isPresentInList(n, n2);
		if (n1 == null) {
			// System.out.println("Invalid Node to be deleted");
			return;

		} else {
			if (n1.getCourses().contains(course2)) {
				n1.getCourses().remove(course2);
				n1.notifyObserver(bnum, course2);

			} else {
				System.out.println("No element found");
			}
		}

	}

	private Node isPresentInList(Node n, Node currentNode) {
		if (currentNode == null) {
			return null;
		}
		if (n.getbNumber() == currentNode.getbNumber()) {
			return currentNode;
		} else if (n.getbNumber() < currentNode.getbNumber()) {
			if (currentNode.getLeft() == null) {
				return null;
			} else {
				return isPresent(n, currentNode.getLeft());
			}
		} else if (n.getbNumber() > currentNode.getbNumber()) {
			if (currentNode.getRight() == null) {
				return null;
			} else {
				return isPresent(n, currentNode.getRight());
			}
		}
		return currentNode;
	}

	public Node getRoot() {
		return root;
	}

	// display elements
	private void displayTree(Node catchingRoot) {
		if (catchingRoot == null) {
			return;
		}
		displayTree(catchingRoot.getLeft());
		displayTree(catchingRoot.right);

	}

	// check for similar items and print them
	public void checkForIdentitalData(Node catchingRoot) {
		logger.printToLog(3, "Inside checkForIdentitalData() method of BST\n");

		if (catchingRoot != null) {
			reverseSortedArray.put(Integer.toString(catchingRoot.getbNumber()), catchingRoot.getCourses().toString());
		}
		if (catchingRoot == null) {
			return;
		}
		checkForIdentitalData(catchingRoot.getLeft());
		checkForIdentitalData(catchingRoot.right);

	}

	public void checkForSimilarity() {

		res.printViaStd("\n************SIMILAR ITEMS***********");
		res.printViaFile("\n************SIMILAR ITEMS***********");
		for (Map.Entry<String, String> entry : reverseSortedArray.entrySet()) {
			for (Map.Entry<String, String> entry1 : reverseSortedArray.entrySet()) {
				if (entry1.getKey() != entry.getKey()) {
					if (entry.getValue().equals(entry1.getValue())) {
						// System.out.println("***Key is : " + entry1.getKey() +
						// " value is : " + entry1.getValue());
						res.printViaStd("Key is : " + entry1.getKey() + " value is : " + entry1.getValue());
						res.printViaFile("Key is : " + entry1.getKey() + " value is : " + entry1.getValue());
					}
				}
			}
		}
	}

}
