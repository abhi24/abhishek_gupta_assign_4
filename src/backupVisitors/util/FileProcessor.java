package backupVisitors.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import backupVisitors.util.Logger.DebugLevel;

public class FileProcessor {
	FileInputStream inputfile;
	FileInputStream deletefile;;
	FileOutputStream outputfile;
	BufferedReader br;
	BufferedReader br1;
	BufferedWriter bw;
	Scanner in = null;
	MyLogger logger = MyLogger.getInstance();

	public FileProcessor(String inputFile, String outputFile) {
		Logger.writeMessage("FileProcessor COnstructor called",DebugLevel.CONSTRUCTOR);
		//logger.printToLog(2, "Inside the FileProcessor constructor\n");
		try {
			inputfile = new FileInputStream(inputFile);
			br = new BufferedReader(new InputStreamReader(inputfile));
		} catch (FileNotFoundException e) {
			System.out.println("The file name is either wrong or the file doesn't exist");
			System.exit(1);
		} finally {

		}
		try {
			outputfile = new FileOutputStream(outputFile);
			bw = new BufferedWriter(new OutputStreamWriter(outputfile));
		} catch (IOException io) {
			System.out.println("Error while writing to file");
		} finally {

		}
	}

	public String ReadLine(String inputFile) {

		String file = inputFile;
		try {
			if (in == null) {
				in = new Scanner(new File(file));
			}

			BufferedReader br = new BufferedReader(new FileReader(file));
			try {
				if (br.readLine() == null) {
					System.out.println("No errors, and file empty");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (in.hasNext()) {
				String curr;
				curr = in.nextLine();
				return curr;
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
		return null;
	}

	public FileProcessor() {

	}

	public String read() {
		try {
			return br.readLine();
		} catch (IOException e) {
			System.out.println("Error in read method");
		} finally {

		}
		return null;
	}
}
