package backupVisitors.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Results implements FileDisplayInterface, StdoutDisplayInterface {

	PrintWriter printWriter;
	int counter = 0;
	private String[] array = new String[10000];
	public String mainarray;
	boolean flag = false;
	FileWriter f;
	static int i = 0;
	File f1 = new File("stats.txt");
	File f2 = new File("sorted.txt");
	File f3 = new File("identical.txt");

	public void printViaStd(String s) {
		System.out.println(s);
	}

	public void printViaFile(String s) {

		createFiles();
		mainarray = (String) (array[counter] = s);
		writeToFile(mainarray);
		counter++;
	}

	private void createFiles() {

	}

	public void writeToFile(String s) {
		if (s.contains("THE COUNT IS")) {
			try {
				f = new FileWriter(f1);
				Files.write(Paths.get("stats" + ".txt"), s.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get("stats" + ".txt"), "\n".getBytes(), StandardOpenOption.APPEND);
				i++;
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		if ((i == 1) && (s.contains("The"))) {
			try {
				Files.write(Paths.get("stats" + ".txt"), s.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get("stats" + ".txt"), "\n".getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (s.contains("SORTED ELEMENTS")) {
			try {
				f = new FileWriter(f2);
				Files.write(Paths.get("sorted" + ".txt"), s.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get("sorted" + ".txt"), "\n".getBytes(), StandardOpenOption.APPEND);
				i++;
			} catch (IOException e) {

				e.printStackTrace();
			}

		}
		if ((i == 2) && (s.contains("Courses are"))) {
			try {
				Files.write(Paths.get("sorted" + ".txt"), s.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get("sorted" + ".txt"), "\n".getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (s.contains("SIMILAR ITEMS")) {
			try {
				f = new FileWriter(f3);

				Files.write(Paths.get("identical" + ".txt"), s.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get("identical" + ".txt"), "\n".getBytes(), StandardOpenOption.APPEND);
				i++;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (i == 3 && (s.contains("Key is"))) {
			try {
				Files.write(Paths.get("identical" + ".txt"), s.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get("identical" + ".txt"), "\n".getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
