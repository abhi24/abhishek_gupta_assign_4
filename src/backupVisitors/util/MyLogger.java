package backupVisitors.util;

import java.io.FileWriter;
import java.io.IOException;

public class MyLogger {
	private volatile static MyLogger myLogger;
	private static String fileName = "Log.txt";
	private FileWriter writer = null;
	private static int DEBUG_VALUE = 0;

	private MyLogger() {
	}

	public static MyLogger getInstance() {
		if (myLogger == null)
			myLogger = new MyLogger();
		return myLogger;
	}

	public void setMyLogger_Value(String value) {
		try {
			int v = Integer.parseInt(value);
			if (v >= 0 && v < 4)
				DEBUG_VALUE = v;
			else {
				System.err.println("Please enter a number between 1 and 3");
				System.exit(-1);
			}
		} catch (NumberFormatException e) {
			System.err.println("Incorrect Format");
			System.exit(-1);
		}
	}

	public void printToLog(int level, String myLoggerMessage) {
		if (level != DEBUG_VALUE)
			return;
		try {
			writer = new FileWriter(fileName, true);
			if (level == DEBUG_VALUE) {
				writer.write(myLoggerMessage);
			}
			writer.close();
		} catch (IOException e) {
			System.exit(-1);
		}
	}
}
