package backupVisitors.visitors;

import backupVisitors.myTree.BST;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.MyLogger;
import backupVisitors.util.Results;

public class StatsVisitor implements TreeProcessingVisitorI {

	FileProcessor fp;
	BST bst;
	MyLogger logger = MyLogger.getInstance();

	public StatsVisitor(FileProcessor fpIn) {
		this.fp = fpIn;
	}

	@Override
	public void visit(Object o) {
		logger.printToLog(1, "Inside the visit() method of StatsVisitor\n");

		Results res = new Results();
		bst = (BST) o;
		int totalStudents = bst.checkNodeStats(bst.getRoot());
		int coursesTotal = bst.checkNodeCoursesStats(bst.getRoot());
		bst.checkNodeCourseWithMedian(bst.getRoot());
		Object median = bst.calculateMedian();

		double averageRecords = ((double) coursesTotal / (double) totalStudents);
		System.out.println("\n************THE COUNT IS***********");
		res.printViaFile("\n************THE COUNT IS***********");

		String total2 = String.valueOf(averageRecords);
		res.printViaStd("The average is : " + total2);
		res.printViaFile("The average is : " + total2);

		res.printViaStd("The median is : " + median);
		res.printViaFile("The median is : " + median);
	}
}
