package backupVisitors.visitors;

import backupVisitors.myTree.BST;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.MyLogger;

public class IdenticalRecordsVisitor implements TreeProcessingVisitorI {
	MyLogger logger = MyLogger.getInstance();

	FileProcessor fp;
	BST bst;

	public IdenticalRecordsVisitor(FileProcessor fpIn) {
		this.fp = fpIn;
	}

	@Override
	public void visit(Object o) {
		logger.printToLog(1, "Inside the visit() method of IdenticalRecordsVisitor\n");

		bst = (BST) o;
		bst.checkForIdentitalData(bst.getRoot());
		bst.checkForSimilarity();
	}

}