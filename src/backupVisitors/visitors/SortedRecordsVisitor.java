package backupVisitors.visitors;

import backupVisitors.myTree.BST;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.MyLogger;

public class SortedRecordsVisitor implements TreeProcessingVisitorI {
	MyLogger logger = MyLogger.getInstance();

	FileProcessor fp;
	BST bst;

	public SortedRecordsVisitor(FileProcessor fpIn) {
		this.fp = fpIn;
	}

	@Override
	public void visit(Object o) {
		logger.printToLog(1, "Inside the visit() method of SortedRecordsVisitor\n");

		bst = (BST) o;
		bst.checkNodeWithSorting(bst.getRoot());
		bst.sortingStart();
	}

}
