package backupVisitors.visitors;

import backupVisitors.myTree.BST;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.MyLogger;
import backupVisitors.util.Results;

public class FullTimeStatusVisitor implements TreeProcessingVisitorI {
	Results res = new Results();
	FileProcessor fp;
	BST bst;
	MyLogger logger = MyLogger.getInstance();


	public FullTimeStatusVisitor(FileProcessor fpIn) {
		this.fp = fpIn;
	}

	@Override
	public void visit(Object o) {
		logger.printToLog(1, "Inside the visit() method of FullTimeStatusVisitor\n");

		bst = (BST) o;
//		res.printViaStd("\n**************LESS NUMBER OF COURSES*************");
	//	System.out.println("\n**************LESS NUMBER OF COURSES*************");
		bst.checkNodeWithDeficencies(bst.getRoot());
	}

}
