package backupVisitors.visitors;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import backupVisitors.myTree.BST;
import backupVisitors.myTree.Node;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.MyLogger;

public class DeleteVisitor implements TreeProcessingVisitorI {
	FileProcessor fp;
	public BST bst;
	String course = null;
	MyLogger logger = MyLogger.getInstance();

	public DeleteVisitor(FileProcessor fpIn) {
		this.fp = fpIn;
	}

	public void visit(Object o) {
		logger.printToLog(1, "Inside the visit() method of DeleteVisitor\n");
		bst = (BST) o;
		while (true) {
			String data = fp.read();
			if (data == null || data == "") {
				break;
			}
			int i = 0;
			Pattern pattern = Pattern.compile("\\w+");
			Matcher matcher = pattern.matcher(data);

			int intVal = 0;

			while (matcher.find()) {

				if (i == 0) {
					intVal = Integer.parseInt(matcher.group());
					i++;
				} else {
					course = matcher.group();
				}

			}
			Node mainNode = new Node(intVal);

			bst.insert(mainNode, course);
		}
	}
}
