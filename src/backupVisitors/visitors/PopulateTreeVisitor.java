package backupVisitors.visitors;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import backupVisitors.myTree.BST;
import backupVisitors.myTree.Node;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.MyLogger;

public class PopulateTreeVisitor implements TreeProcessingVisitorI {
	FileProcessor fp;
	public BST bst;
	String course = null;
	public List<BST> backupList;
	Node observerNode = null;
	Node mainNode1;
	MyLogger logger = MyLogger.getInstance();

	public PopulateTreeVisitor(FileProcessor fpIn) {
		this.fp = fpIn;
		bst = new BST();
		backupList = new ArrayList<BST>();
		BST backup;
		for (int i = 0; i < 2; i++) {
			backup = new BST();
			backupList.add(backup);
		}
	}

	public void visit(Object o) {
		logger.printToLog(1, "Inside the visit() method of PopulateTreeVisitor\n");

		bst = (BST) o;
		while (true) {
			String data = fp.read();
			if (data == null || data == "") {
				break;
			}
			int i = 0;
			Pattern pattern = Pattern.compile("\\w+");
			Matcher matcher = pattern.matcher(data);

			int intVal = 0;

			while (matcher.find()) {

				if (i == 0) {
					intVal = Integer.parseInt(matcher.group());
					i++;
				} else {
					course = matcher.group();
				}

			}
			Node mainNode = new Node(intVal);
			mainNode = new Node(intVal, null);
			bst.insert(mainNode, course);
			// mainNode = new Node(intVal, null);
			for (BST b : backupList) {
				observerNode = new Node(intVal, mainNode);
				b.insert(observerNode, course);
			}
		}
	}
}
